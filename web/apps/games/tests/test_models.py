from unittest import skip

from django.test import TestCase

from apps.games.models import *

User = get_user_model()


class DeveloperModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.developer_obj = Developer.objects.create(title='First')

    @skip('No need unless not needed')
    def test_title_label(self):
        field_label = self.developer_obj._meta.get_field('title').verbose_name
        self.assertEquals(field_label, 'Developer')

    def test_object_name(self):
        self.assertEquals(str(self.developer_obj), 'First')


class PublisherModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.publisher_obj = Publisher.objects.create(title='First')

    @skip('No need unless not needed')
    def test_title_label(self):
        field_label = self.publisher_obj._meta.get_field('title').verbose_name
        self.assertEquals(field_label, 'Publisher')

    def test_object_name(self):
        self.assertEquals(str(self.publisher_obj), 'First')


class GenreModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.genre_obj = Genre.objects.create(title='F-i)r*s_異t[!@ u', description='first')

    def test_title_label(self):
        field_label = self.genre_obj._meta.get_field('title').verbose_name
        self.assertEquals(field_label, 'Title')

    def test_object_name(self):
        self.assertEquals(str(self.genre_obj), 'F-i)r*s_異t[!@ u')

    def test_slug_field(self):
        self.assertEquals(self.genre_obj.slug, 'f-irs_t-u')

    def test_description_max_length(self):
        max_length = self.genre_obj._meta.get_field('description').max_length
        self.assertEquals(max_length, 2500)

    def test_get_absolute_url(self):
        self.assertEquals(self.genre_obj.get_absolute_url(), '/games/f-irs_t-u/')


class SpecialModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        start_date = timezone.datetime(2021, 1, 1)
        end_date = timezone.datetime(2021, 1, 2)
        cls.special_obj = Special.objects.create(title='First special offer', discount_percentage=20,
                                                 start_date=start_date, end_date=end_date)

    def test_labels(self):
        field_label = self.special_obj._meta.get_field('title').verbose_name
        self.assertEquals(field_label, 'Title')
        field_label = self.special_obj._meta.get_field('discount_percentage').verbose_name
        self.assertEquals(field_label, 'Discount percentage')
        field_label = self.special_obj._meta.get_field('start_date').verbose_name
        self.assertEquals(field_label, 'Start date of the discount')
        field_label = self.special_obj._meta.get_field('end_date').verbose_name
        self.assertEquals(field_label, 'End date of the discount')

    def test_object_name(self):
        self.assertEquals(str(self.special_obj),
                          f'special offer "{self.special_obj.title}" expires on the {self.special_obj.end_date}')


class GameModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.release_date = timezone.datetime(2021, 1, 2)
        cls.game_obj1 = Game.objects.create(title='Fir st', description='first', release_date=cls.release_date)

    def test_object_name(self):
        self.assertEquals(str(self.game_obj1), 'Fir st')

    def test_slug_field(self):
        self.assertEquals(self.game_obj1.slug, 'fir-st')

    def test_description_max_length(self):
        max_length = self.game_obj1._meta.get_field('description').max_length
        self.assertEquals(max_length, 4000)

    def test_default_image_path(self):
        path = self.game_obj1._meta.get_field('image').default
        self.assertEquals(path, 'games/images/default/default_header.jpg')

    def test_get_absolute_url(self):
        self.assertEquals(self.game_obj1.get_absolute_url(), '/games/fir-st/')


class ImageModelTest(TestCase):
    pass


class VideoModelTest(TestCase):
    pass


class PriceModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        start_date = timezone.datetime(2021, 1, 1)
        end_date = timezone.datetime(2021, 1, 2)
        cls.special_obj = Special.objects.create(title='First special offer', discount_percentage=20,
                                                 start_date=start_date, end_date=end_date)

        cls.release_date = timezone.datetime(2021, 1, 2)
        cls.game_obj1 = Game.objects.create(title='First', description='first', release_date=cls.release_date)
        cls.game_obj2 = Game.objects.create(title='Second', description='second', release_date=cls.release_date)
        cls.game_obj3 = Game.objects.create(title='Third', description='third', release_date=cls.release_date)
        cls.game_obj4 = Game.objects.create(title='Fourth', description='fourth', release_date=cls.release_date)
        cls.price_obj1 = Price.objects.create(game=cls.game_obj1, price=100)
        cls.price_obj2 = Price.objects.create(game=cls.game_obj2, price=100, special=cls.special_obj)
        cls.price_obj3 = Price.objects.create(game=cls.game_obj3, price=100, discount_price=80)
        cls.price_obj4 = Price.objects.create(game=cls.game_obj4, price=100, discount_price=70, special=cls.special_obj)

    def test_object_name(self):
        self.assertEquals(str(self.price_obj1),
                          f'game "{self.price_obj1.game}" price: {self.price_obj1.price} (discount price: {self.price_obj1.discount_price})')

    def test_get_current_price(self):
        self.assertEquals(self.price_obj1.get_current_price(), 100)
        self.assertEquals(self.price_obj2.get_current_price(), 80)
        self.assertEquals(self.price_obj3.get_current_price(), 80)
        self.assertEquals(self.price_obj4.get_current_price(), 70)

    def test_calculate_discount_price(self):
        self.assertEquals(Price.calculate_discount_price(Decimal(100), Decimal('20')), Decimal('80'))
        self.assertEquals(Price.calculate_discount_price(Decimal(100), Decimal('21.01')), Decimal('78.99'))

    def test_set_discount_price(self):
        self.assertEquals(self.price_obj1.set_discount_price(), None)
        self.assertEquals(self.price_obj2.set_discount_price(), 80)
        self.assertEquals(self.price_obj3.set_discount_price(), 80)
        self.assertEquals(self.price_obj4.set_discount_price(), 70)

    def test_reset_discount_prices(self):
        self.assertEquals(Price.reset_discount_prices(self.special_obj.pk), 2)


class SystemRequirementModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.release_date = timezone.datetime(2021, 1, 2)
        cls.game_obj = Game.objects.create(title='First', description='first', release_date=cls.release_date)
        cls.sys_req_obj = SystemRequirement.objects.create(game=cls.game_obj, os='Windows',
                                                           processor='Intel Core i5 or AMD equivalent', memory=8,
                                                           graphic='NVIDIA GTX 660 or AMD Radeon HD 7950',
                                                           directx='Version 11', network='yes', storage=2,
                                                           additional_notes='Gamepad Recommended')

    def test_object_name(self):
        self.assertEquals(str(self.sys_req_obj), f'system requirements for {self.sys_req_obj.game}')

    def test_get_network_display_label(self):
        self.assertEquals(self.sys_req_obj.get_network_display(), 'Broadband Internet connection')


class CommentModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user_obj = User.objects.create(username='vitos', password='gsgs23kk')
        cls.release_date = timezone.datetime(2021, 1, 2)
        cls.game_obj = Game.objects.create(title='First', description='first', release_date=cls.release_date)
        cls.comment_obj = Review.objects.create(user=cls.user_obj, game=cls.game_obj, text='commentary')

    def test_object_name(self):
        self.assertEquals(str(self.comment_obj), f'comment by {self.comment_obj.user} on {self.comment_obj.created}')
