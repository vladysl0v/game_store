# Generated by Django 3.1.1 on 2020-09-20 15:55

from decimal import Decimal
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0014_auto_20200920_1636'),
    ]

    operations = [
        migrations.AlterField(
            model_name='price',
            name='discount_price',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=9, null=True, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))], verbose_name='Discount price'),
        ),
        migrations.AlterField(
            model_name='price',
            name='price',
            field=models.DecimalField(decimal_places=2, help_text='price currency in USD', max_digits=9, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))], verbose_name='Price'),
        ),
    ]
