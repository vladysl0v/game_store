# Generated by Django 3.1.1 on 2020-09-20 17:38

from decimal import Decimal
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0015_auto_20200920_1855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='price',
            name='price',
            field=models.DecimalField(decimal_places=2, help_text='enter a value in USD', max_digits=9, validators=[django.core.validators.MinValueValidator(Decimal('0.01'))], verbose_name='Price'),
        ),
        migrations.AlterField(
            model_name='special',
            name='discount_percentage',
            field=models.DecimalField(decimal_places=0, help_text='40 equal 40%, max value 99', max_digits=2, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(99)], verbose_name='Discount percentage'),
        ),
    ]
