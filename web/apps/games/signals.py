from django.db.models.signals import pre_delete, post_delete
from django.dispatch import receiver
from django.core.files.storage import default_storage

from apps.games.models import Price, Special, Image, Video
from apps.games.tasks import delete_file_from_storage_task


@receiver(pre_delete, sender=Special)
def reset_discount_prices_signal(sender, instance, **kwargs):
    """ I use signal instead "model delete" method, cuz using action delete in admin pannel doesn't call model delete method """
    Price.reset_discount_prices(instance.pk)


@receiver(post_delete, sender=Video)
@receiver(post_delete, sender=Image)
def delete_file_from_storage_signal(sender, instance, **kwargs):
    """ after deleting an object record, deletes the media file from storage """
    if isinstance(instance, Image):
        path = instance.image.path
    else:
        path = instance.video.path

    # if default_storage.exists(path):
    #     delete_file_from_storage_task.delay(path)
