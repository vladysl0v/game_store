from __future__ import absolute_import, unicode_literals

import time
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import default_storage
from django.utils import timezone

from apps.games.models import Special, Price
from config.celery import app
from celery import shared_task
from celery.task import periodic_task
from celery.schedules import crontab


@app.task(ignore_result=True, typing=False)
def delete_file_from_storage_task(path: str):
    """ removes a file from DEFAULT storage """
    default_storage.delete(path)


@app.task(ignore_result=True, typing=False)
def reset_discount_prices_task(pk: int):
    """ set None for all associated (with Special) discount prices """
    try:
        Special.objects.get(pk=pk)
        Price.reset_discount_prices(pk)
    except ObjectDoesNotExist:
        print('Special does not exist anymore')  # todo logging


@periodic_task(run_every=(timezone.timedelta(minutes=60)))
def assign_to_reset_discount_prices_task():
    instances = Special.get_pre_expired_specials().values('pk', 'end_date')
    for instance in instances:
        try:
            end = instance['end_date']
            reset_discount_prices_task.apply_async((instance.get('pk'),), eta=end)
        except KeyError:
            print('really??')  # todo logging


# @periodic_task(run_every=(timezone.timedelta(minutes=2)))
# def raise_val_error():
#     raise ValueError
#
#
# @periodic_task(run_every=(timezone.timedelta(minutes=4)))
# def raise_key_error():
#     raise KeyError
