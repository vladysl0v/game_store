from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ModelViewSet

from apps.activities.models import Review
from apps.activities.serializers import ReviewSerializer


class ReviewModelViewSet(ModelViewSet):
    queryset = Review.objects.filter(parent=None)
    serializer_class = ReviewSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
