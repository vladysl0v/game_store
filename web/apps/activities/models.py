from django.contrib.auth import get_user_model
from django.db import models
from model_utils import Choices
from model_utils.models import TimeStampedModel

from apps.games.models import Game
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class Vote(TimeStampedModel):
    VOTE_TYPE = Choices(
        ('u', _('Up')),
        ('d', _('Down')),
        ('f', _('Funny')),
    )
    type = models.CharField(_('Vote type'), max_length=1, choices=VOTE_TYPE)

    class Meta:
        abstract = True
        ordering = ('-modified',)


class Review(TimeStampedModel):
    """ Review to the Game or review on review. """
    REVIEW_TYPE = Choices(
        ('p', _('Positive')),
        ('n', _('Negative')),
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reviews')
    game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='reviews')
    text = models.TextField(max_length=3000)
    type = models.CharField(_('Review type'), max_length=1, choices=REVIEW_TYPE)
    parent = models.ForeignKey('self', blank=True, null=True, related_name='replies', on_delete=models.CASCADE)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'review by {self.user} on {self.created}'

    class Meta:
        ordering = ('-created',)


class GameVote(Vote):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='gamevotes')
    game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='votes')


class ReviewVote(Vote):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reviewvotes')
    review = models.ForeignKey(Review, on_delete=models.CASCADE, related_name='votes')


